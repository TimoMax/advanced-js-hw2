// try - catch може бути корисним наприклад тоді коли ми робимо запит на сервер 
// і з якихось причин повертається помилка. це дозволить продовжити роботу кода на зупиняючи його


const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const div = document.querySelector('#root')

function Selection(books) {
  this.render = function () {
    const ul = document.createElement('ul');

    books.forEach((elem) => {
      try {

        if (!elem.hasOwnProperty('author')) {
          throw new SyntaxError('author-undefined');
        }
        if (!elem.hasOwnProperty('name')) {
          throw new SyntaxError('name-undefined');
        }
        if (!elem.hasOwnProperty('price')) {
          throw new SyntaxError('price-undefined');
        }

        const li = document.createElement('li');
        li.textContent = `AUTHOR: ${elem.author}; BOOK NAME: ${elem.name}; BOOK PRICE: ${elem.price}`;



        ul.append(li);
      } catch (err) {
        console.error(err.message);
      }
    });

    div.append(ul);
  };
}

const library = new Selection(books);
library.render(div);
